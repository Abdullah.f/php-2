<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Function</title>
</head>
<body>
    <h1>Berlatih Function PHP</h1>
    <?php
        echo "<h3> Soal No 1 Greetings </h3>";
        /*
        Soal No 1
        Greetings
        Buatlah sebuah function greetings() yang menerima satu parameter berupa string.

        contoh: greetings("abduh");
        Output: "Halo Abduh, Selamat Datang di PKS Digital School!"
        */
        function greetings($name){
            echo "Halo ".$name.", Selamat Datang di PKS Digital School!"."<br><br>";
        }
        greetings("Bagas");
        greetings("Wahyu");
        greetings("Abdul");


        echo "<h3>Soal No 2 Reverse String</h3>";
        /*
        Soal No 2
        Reverse String
        Buatlah sebuah function reverseString() untuk mengubah string berikut menjadi kebalikannya
        menggunakan function dan looping (for/while/do while).
        Function reverseString menerima satu parameter berupa string.
        NB: DILARANG menggunakan built-in function PHP sepert strrev(), HANYA gunakan LOOPING!

        reverseString("abdul");
        Output: ludba
        */
        function reverse($word1){
            $wordlength = strlen($word1);
            $patch = "";
            for ($i = ($wordlength - 1); $i >= 0; $i--){
                $patch .= $word1[$i];
            }
            return $patch;
        }

        function reverseString($word2){
            $string = reverse($word2);
            echo $string."<br><br>";
        }
        reverseString("abduh");
        reverseString("Digital School");
        reverseString("We Are PKS Digital School Developers");


        echo "<h3>Soal No 3 Palindrome </h3>";
        /*
        Soal No 3
        Palindrome
        Buatlah sebuah function yang menerima parameter berupa string yang mengecek apakah string tersebut sebuah palindrome atau bukan.
        Palindrome adalah sebuah kata atau kalimat yang jika dibalik akan memberikan kata yang sama contohnya: katak, civic.
        Jika string tersebut palindrome maka akan mengembalikan nilai true, sedangkan jika bukan palindrome akan mengembalikan false.

        Contoh:
        palindrome("katak") => output : "true"
        palindrome("jambu") => output : "false"
        NB: DILARANG menggunakan built-in function PHP seperti strrev() dll. Gunakan looping seperti biasa atau gunakan function reverseString dari
        jawaban no.2!
        */
        function reverse2($word1){
            $wordlength = strlen($word1);
            $patch = "";
            for ($i = ($wordlength - 1); $i >= 0; $i--){
                $patch .= $word1[$i];
            }
            return $patch;
        }

        function palindrome($word2){
            $string = reverse2($word2);
            if ($string == $word2){
                echo "palindrome(".$word2.") => ".$string." : true<br><br>";
            } else {
                echo "palindrome(".$word2.") => ".$string." : false<br><br>";
            }
        }
        palindrome("civic") ; // true
        palindrome("nababan") ; // true
        palindrome("jambaban"); // false
        palindrome("racecar"); // true


        echo "<h3>Soal No 4 Tentukan Nilai </h3>";
        /*
        Soal 4
        Buatlah sebuah function bernama tentukan_nilai.
        Di dalam function tentukan_nilai yang menerima parameter berupa integer dengan ketentuan,
        jika paramater integer >= 85 dan <= 100 maka akan mereturn String “Sangat Baik”,
        Selain itu jika parameter integer >= 70 dan < 85 maka akan mereturn string “Baik”,
        Selain itu jika parameter integer >= 60 dan < 70 maka akan mereturn string “Cukup”,
        Selain itu maka akan mereturn string “Kurang”.
        */
        function tentukan_nilai($nilai){
            $output = "";
            if ($nilai >= 85 && $nilai <= 100){
                $output .= "Sangat Baik";
            }else if ($nilai >= 70 && $nilai < 85){
                $output .= "Baik";
            }else if ($nilai >= 60 && $nilai < 70){
                $output .= "Cukup";
            }else {
                $output .="Kurang";
            }
            return $output."<br><br>";
        }
        echo "Nilai 98 : ".tentukan_nilai(98); //Sangat Baik
        echo "Nilai 76 : ".tentukan_nilai(76); //Baik
        echo "Nilai 67 : ".tentukan_nilai(67); //Cukup
        echo "Nilai 43 : ".tentukan_nilai(43); //Kurang
    ?>
</body>
</html>